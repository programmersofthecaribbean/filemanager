﻿using System;
using System.IO;
using System.Collections.Generic;

namespace FileManager
{
    struct EntryInfo
    {
        public string name;
        public string lastMod;
        public string type;
        public string size;

        public string[] getFields()
        {
            string[] fields = new string[4];
            fields[0] = name;
            fields[1] = lastMod;
            fields[2] = type;
            fields[3] = size;
            return fields;
        }
    }

    static class FileManager
    {
        public static string currentDir = string.Empty;
        public static bool isRoot = true;

        /*
         * Predicates for interaction with user. Must be implemented in programm.
         */
        public static Predicate<string> ask;
        public static Action<string> showMessage;

        /*
         * strings for output messages
         */
        const string _remove_files = "Do you really want to delete selected file(s)?";
        const string _del_dir_recursively = "Directory is not empty. Do you want to delete it with all content?";
        const string _merge_dirs = "Directory already exists. Do you want to merge content?";
        const string _entry_exists = "Entry already exists";
        const string _overwrite_file = "File already exists. Do you want to overwrite file?";
        const string _invalid_name = "Entry name is not valid";
        const string _invalid_characters = "New name contains disallowed characters";


        /*
         * 
         * Methods for getting information
         * 
         */
        public static EntryInfo[] getDrives()
        {
            var drives = System.IO.DriveInfo.GetDrives();
            EntryInfo[] entries = new EntryInfo[drives.Length];
            for (int i = 0; i < drives.Length; i++)
            {
                entries[i].name = drives[i].Name;
                entries[i].lastMod = string.Empty;
                entries[i].type = drives[i].DriveType.ToString();
                try
                {
                    entries[i].size = FileManager.getSize(drives[i].TotalSize);
                }
                catch (Exception)
                {
                    entries[i].size = string.Empty;
                }
            }
            return entries;
        }

        public static EntryInfo[] openDirectory(string path)
        {
            string[] content;
            string save_cur_dir = currentDir;
            setCurrentDir(Path.Combine(@currentDir, @path));
            try
            {
                content = Directory.GetFileSystemEntries(@currentDir);
            }
            catch (Exception)
            {
                setCurrentDir(save_cur_dir);
                return null;
            }
            EntryInfo[] entries = new EntryInfo[content.Length];
            for (int i = 0; i < content.Length; i++)
            {
                entries[i].name = content[i];
                entries[i].lastMod = File.GetLastAccessTime(content[i]).ToString("dd.MM.yyyy hh:mm");
                if (isDirectory(content[i]))
                {
                    entries[i].name = new DirectoryInfo(content[i]).Name;
                    entries[i].type = "folder";
                }
                else
                {
                    entries[i].name = Path.GetFileName(content[i]);
                    FileInfo file = new FileInfo(content[i]);
                    if (file.Extension.Length == 0)
                        entries[i].type = "file";
                    else
                        entries[i].type = file.Extension.Remove(0, 1).ToUpper() + " file";
                    entries[i].size = getSize(new System.IO.FileInfo(content[i]).Length);
                }
            }
            return entries;
        }

        public static string[] getDirectoryContent(string path)
        {
            isRoot = false;
            setCurrentDir(path);
            var content = Directory.GetFileSystemEntries(@path);
            string[] entries = new string[content.Length];
            for (int i = 0; i < content.Length; i++)
            {
                entries[i] = Path.GetFileName(content[i]);
            }
            return entries;
        }

        public static EntryInfo[] getParent()
        {
            var current = new DirectoryInfo(currentDir);
            if (current.Equals(current.Root) || current.Parent == null)
            {
                setCurrentDir(string.Empty);
                return getDrives();
            }
            isRoot = false;
            var parent = current.Parent;
            setCurrentDir(parent.FullName);
            return openDirectory(currentDir);
        }

        public static void setCurrentDir(string path)
        {
            currentDir = path;
            if (string.IsNullOrEmpty(currentDir))
            {
                isRoot = true;
                return;
            }
            isRoot = false;
            try
            {
                Directory.SetCurrentDirectory(path);
            }
            catch (Exception ex)
            {
                showMessage(ex.Message);
            }
        }

        public static string getSize(long bytes)
        {
            string result = string.Empty;
            if (bytes < 1024)
                return bytes.ToString();
            bytes /= 1024;
            if (bytes < 1024)
                return bytes + " KiB";
            bytes /= 1024;
            if (bytes < 1024)
                return bytes + " MiB";
            bytes /= 1024;
            return bytes + " GiB";
        }

        public static string generateUniqueCopyName(string entry, string path)
        {
            string new_entry = Path.Combine(path, entry);
            int counter = 0;
            while (entryExists(new_entry))
            {
                counter++;
                new_entry = Path.Combine(path, "(copy " + counter + ") " + entry);
            }
            return new_entry;
        }

        public static string generateUniqueDirectoryName(string entry, string path)
        {
            string new_entry = Path.Combine(path, entry);
            string copy_suffix = "";
            int counter = 0;
            while (entryExists(new_entry))
            {
                counter++;
                copy_suffix = " (" + counter + ")";
                new_entry = Path.Combine(path, entry + copy_suffix);
            }
            return entry + copy_suffix;
        }

        /*
         * 
         * Entry manipulation methods
         * 
         */
        public static bool createEntry(string entry, bool isDirectory = true)
        {
            if (!isValidName(entry))
            {
                showMessage(_invalid_name + ":\n" + entry);
                return false;
            }
            if (isDirectory)
                Directory.CreateDirectory(@entry);
            else
                File.Create(@entry);
            return true;
        }

        public static void renameEntry(string path, string newname)
        {
            if (!isValidName(newname))
            {
                showMessage(_invalid_name);
                return;
            }
            if (isDirectory(path))
            {
                if (Directory.Exists(newname) && ask(_merge_dirs))
                {
                    var entries = Directory.GetFileSystemEntries(path);
                    moveEntry(entries, newname);
                    Directory.Delete(path);
                }
                else
                    Directory.Move(path, newname);
            }
            else
            {
                if (File.Exists(newname))
                    if (ask(_overwrite_file))
                        File.Delete(newname);
                    else
                        return;
                File.Move(path, newname);
            }
        }

        public static void deleteEntry(string[] entries, bool ask_del = false)
        {
            string entry;
            foreach (var item in entries)
            {
                entry = Path.Combine(currentDir, item);
                if (isDirectory(entry))
                {
                    if (!isDirectoryEmpty(entry) && !ask_del)
                        if (!ask(_del_dir_recursively))
                            return;
                    try
                    {
                        Directory.Delete(entry, true);
                    }
                    catch (Exception e) { showMessage(e.Message); }
                }
                else
                {
                    try
                    {
                        File.Delete(entry);
                    }
                    catch (Exception e) { showMessage(e.Message); }
                }
            }
        }

        public static void copyEntry(string[] source, string path)
        {
            string sourceEntry, destEntry, save_cur_dir;
            bool same_dir = currentDir.Equals(path);
            save_cur_dir = currentDir;

            foreach (var entry in source)
            {
                sourceEntry = Path.Combine(currentDir, entry);
                destEntry = Path.Combine(path, entry);

                if (!isDirectory(sourceEntry))
                {
                    if (File.Exists(destEntry))
                        if (same_dir || !ask(_overwrite_file))
                            destEntry = generateUniqueCopyName(@entry, @path);
                    try
                    {
                        File.Copy(@sourceEntry, @destEntry, true);
                    }
                    catch (Exception e) { showMessage("Cannot copy file. " + e.Message); }
                }
                else
                {
                    if (!Directory.Exists(destEntry))
                        Directory.CreateDirectory(destEntry);
                    else
                    {
                        if (same_dir || !ask(_overwrite_file))
                            destEntry = generateUniqueDirectoryName(entry, path);
                    }
                    copyEntry(getDirectoryContent(@sourceEntry), destEntry);
                }
                setCurrentDir(save_cur_dir);
            }            
        }

        public static void moveEntry(string[] source, string path)
        {
            copyEntry(source, path);
            deleteEntry(source, true);
        }

        /*
         * 
         * Methods for some checks
         * 
         */
        public static bool isDirectory(string path)
        {
            try
            {
                FileAttributes attr = File.GetAttributes(path);
                return attr.HasFlag(FileAttributes.Directory);
            }
            catch (Exception) { return false; }
        }

        public static bool isSystem(string path)
        {
            string entry = Path.Combine(currentDir, path);
            try
            {
                FileAttributes attr = File.GetAttributes(entry);
                return attr.HasFlag(FileAttributes.System);
            }
            catch (Exception) { return false; }
        }

        public static bool isDirectoryEmpty(string path)
        {
            IEnumerable<string> items = Directory.EnumerateFileSystemEntries(path);
            using (IEnumerator<string> en = items.GetEnumerator())
            {
                return !en.MoveNext();
            }
        }

        public static bool entryExists(string path)
        {
            return File.Exists(path) || Directory.Exists(path);
        }

        public static bool isValidName(string name)
        {
            bool isValid = !string.IsNullOrEmpty(name) && (name.IndexOfAny(Path.GetInvalidFileNameChars()) < 0);
            if (!isValid)
                showMessage(_invalid_characters);
            else
                if (entryExists(@name))
                {
                    showMessage(_entry_exists);
                    return false;
                }
            return isValid;
        }
    }
}
