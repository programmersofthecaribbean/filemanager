﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace FileManager
{
    public partial class Form_Main : Form
    {
        ColorDialog askColor = new ColorDialog();
        Color background_color;
        Color text_color;
        FontDialog askFont = new FontDialog();
        Font text_font;

        /*
         * 
         * COMMANDS FOR FILESYSTEM MANAGEMENT
         * 
         */

        void initCommands()
        {
            commandlist.Add(new ToolStripButton("F1 Left", null, set_focus_left));
            commandlist.Add(new ToolStripButton("F2 Right", null, set_focus_right));
            commandlist.Add(new ToolStripButton("F3 View", null, view_file));
            commandlist.Add(new ToolStripButton("F4 Rename", null, rename_entry));
            commandlist.Add(new ToolStripButton("F5 Copy", null, copy_content));
            commandlist.Add(new ToolStripButton("F6 Move", null, move_content));
            commandlist.Add(new ToolStripButton("F7 Delete", null, delete_content));
            commandlist.Add(new ToolStripButton("F8 New folder", null, create_dir));
        }

        void set_focus_left(Object sender, EventArgs args)
        {
            if (!listView1.Focused)
                listView1.Focus();
            FileManager.setCurrentDir(leftpath);
        }

        void set_focus_right(Object sender, EventArgs args)
        {
            if (!listView2.Focused)
                listView2.Focus();
            FileManager.setCurrentDir(rightpath);
        }

        void view_file(Object sender, EventArgs args)
        {
            string abs_path;
            ListView listview = listView1.Focused ? listView1 : listView2;
            var items = listview.SelectedItems;
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Text.Equals(_parent_dir))
                    continue;
                if (FileManager.isDirectory(items[i].Text))
                    continue;
                abs_path = Path.GetFullPath(@items[i].Text);
                System.Diagnostics.Process.Start(@abs_path);
            }
        }

        void rename_entry(Object sender, EventArgs args)
        {
            ListView listview = listView1.Focused ? listView1 : listView2;
            if (!FileManager.isRoot && listview.Items[0].Selected)
                listview.Items[0].Selected = false;
            var items = listview.SelectedItems;
            if (items.Count == 0 || !listview.LabelEdit)
                return;
            else
                items[0].BeginEdit();
        }

        void copy_content(Object sender, EventArgs args)
        {
            bool panel = listView1.Focused;
            ListView listview = panel ? listView1 : listView2;
            if (!FileManager.isRoot && listview.Items[0].Selected)
                listview.Items[0].Selected = false;
            var items = listview.SelectedItems;

            if (items.Count == 0 || string.IsNullOrEmpty(leftpath) || string.IsNullOrEmpty(rightpath))
                return;

            string source = panel ? leftpath : rightpath;
            string dest = panel ? rightpath : leftpath;
            string[] entries = new string[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                entries[i] = items[i].Text;
            }

            FileManager.showMessage("Copying " + items.Count + " item(s) from '" + source + "'\nto '" + dest + "'");
            FileManager.setCurrentDir(source);
            FileManager.copyEntry(entries, dest);
            refresh(!panel);
            if (source.Equals(dest))
                refresh(panel);
            listview.Focus(); // Restore focus
        }

        void move_content(Object sender, EventArgs args)
        {
            bool panel = listView1.Focused;
            ListView listview = panel ? listView1 : listView2;
            if (!FileManager.isRoot && listview.Items[0].Selected)
                listview.Items[0].Selected = false;
            var items = listview.SelectedItems;

            if (items.Count == 0 || string.IsNullOrEmpty(leftpath) || string.IsNullOrEmpty(rightpath))
                return;

            string source = panel ? leftpath : rightpath;
            string dest = panel ? rightpath : leftpath;
            string[] entries = new string[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                entries[i] = items[i].Text;
            }

            FileManager.showMessage("Moving " + items.Count + " item(s) from '" + source + "'\nto '" + dest + "'");
            FileManager.setCurrentDir(source);
            FileManager.moveEntry(entries, dest);
            refresh(!panel);
            refresh(panel);
            listview.Focus(); // Restore focus
        }

        void delete_content(Object sender, EventArgs args)
        {
            bool panel = listView1.Focused;
            bool del_opened_dir = false;
            ListView listview = panel ? listView1 : listView2;
            string neighbor_path = !panel ? leftpath : rightpath;
            neighbor_path += "\\";

            if (!FileManager.isRoot && listview.Items[0].Selected)
                listview.Items[0].Selected = false;
            var items = listview.SelectedItems;
            string source = panel ? leftpath : rightpath;

            if (items.Count == 0 || FileManager.isRoot)
                return;

            string[] entries = new string[items.Count];
            for (int i = 0; i < items.Count; i++)
            {
                entries[i] = items[i].Text;
                if (neighbor_path.Contains(Path.Combine(FileManager.currentDir, entries[i]) + "\\"))
                    del_opened_dir = true;
            }

            if (!FileManager.ask("Do you really want to delete selected item(s)?"))
                return;
            FileManager.showMessage("Deleting " + items.Count + " item(s) from '" + source);
            FileManager.setCurrentDir(source);
            FileManager.deleteEntry(entries);
            refresh(panel);
            if (leftpath.Equals(rightpath))
                refresh(!panel);
            if (del_opened_dir)
            {
                int index = neighbor_path.IndexOf(":");
                string drive = neighbor_path.Substring(0, index + 1);
                loadEntries(FileManager.openDirectory(""), !panel);
            }
            listview.Focus(); // Restore focus
        }

        void create_dir(Object sender, EventArgs args)
        {
            bool panel = listView1.Focused;
            ListView listview = panel ? listView1 : listView2;
            string path = panel ? leftpath : rightpath;
            if (string.IsNullOrEmpty(path))
                return;

            FileManager.setCurrentDir(path);
            string new_folder = FileManager.generateUniqueDirectoryName("New_folder", path);
            if (!FileManager.createEntry(new_folder))
                return;
            listview.SelectedItems.Clear();
            ListViewItem item = new ListViewItem(new string[] { new_folder, DateTime.Now.ToString("dd.MM.yyyy HH:mm"), "folder", "" }, "folder");
            listview.Items.Add(item);
            listview.Items[listview.Items.Count - 1].Selected = true;
            rename_entry(sender, args);
        }

        void run_commandline(Object sender, EventArgs args)
        {
            string command = textBox1.Text;
            if (string.IsNullOrEmpty(command))
                return;

            System.Diagnostics.Process pProcess = new System.Diagnostics.Process();

            pProcess.StartInfo.FileName = "cmd.exe";
            pProcess.StartInfo.Arguments = "/c " + command;
            pProcess.StartInfo.UseShellExecute = false;
            pProcess.StartInfo.RedirectStandardOutput = true;
            pProcess.StartInfo.CreateNoWindow = true;
            pProcess.StartInfo.StandardOutputEncoding = System.Text.Encoding.GetEncoding(866);
            if (!string.IsNullOrEmpty(FileManager.currentDir))
                pProcess.StartInfo.WorkingDirectory = FileManager.currentDir;
            else
                pProcess.StartInfo.WorkingDirectory = Directory.GetCurrentDirectory();
            pProcess.Start();
            string output = pProcess.StandardOutput.ReadToEnd();
            pProcess.WaitForExit();
            if (!string.IsNullOrEmpty(output))
                FileManager.showMessage(output);
        }

        /*
         * 
         * COMMANDS FOR VIEW CUTOMISATION
         * 
         */
        void initCustomization()
        {
            background_color = listView1.BackColor;
            text_color = listView1.ForeColor;
            text_font = listView1.Font;
            askFont.MinSize = 8;
            askFont.MaxSize = 16;
            askFont.ShowEffects = false;
            askFont.AllowScriptChange = false;
        }

        void set_color_background(Object sender, EventArgs args)
        {
            bool panel = listView1.Focused;
            if (askColor.ShowDialog() == DialogResult.OK)
                background_color = askColor.Color;

            // repaint component
            refresh(_LEFT);
            refresh(_RIGHT);

            // restore focus
            if (panel == _LEFT)
                set_focus_left(sender, args);
            else
                set_focus_right(sender, args);
        }

        void set_color_text(Object sender, EventArgs args)
        {
            bool panel = listView1.Focused;
            if (askColor.ShowDialog() == DialogResult.OK)
                text_color = askColor.Color;

            // repaint component
            refresh(_LEFT);
            refresh(_RIGHT);

            // restore focus
            if (panel == _LEFT)
                set_focus_left(sender, args);
            else
                set_focus_right(sender, args);
        }

        void set_text_font(Object sender, EventArgs args)
        {
            if (askFont.ShowDialog() == DialogResult.OK)
                text_font = askFont.Font;
            listView1.Font = text_font;
            listView2.Font = text_font;
        }

        /*
         * 
         * COMMANDS FOR USER MANAGEMENT
         * 
         */
        void show_log_in_panel(Object sender, EventArgs args)
        {
            log_in_form.ShowDialog();
        }

        void log_out(Object sender, EventArgs args)
        {
            if (!FileManager.ask("Do you want to log out?"))
                return;
            currentUser = GUEST;
            this.Text = "FileManager";
            logInToolStripMenuItem.Text = "Log in";
            logInToolStripMenuItem.Click -= log_out;
            logInToolStripMenuItem.Click += show_log_in_panel;
            showAllowedCommands();
        }

        public void set_logined_user(User current)
        {
            currentUser = current;
            this.Text = "FileManager - " + currentUser.name;
            logInToolStripMenuItem.Text = "Log out";
            logInToolStripMenuItem.Click -= show_log_in_panel;
            logInToolStripMenuItem.Click += log_out;
            showAllowedCommands();
        }

        void show_admin_panel(Object sender, EventArgs args)
        {
            Form_admin_panel admin_form = new Form_admin_panel(this);
            admin_form.ShowDialog();
        }

        void change_password(Object sender, EventArgs args)
        {
            string password = "", confirm = "";
            bool ok = true;
            do
            {
                if (Form_Login.InputPasswordBox("Change password", "Enter your new password: ", ref password) != DialogResult.OK)
                    return;
                if (!User.isValidString(password))
                {
                    FileManager.showMessage("New password is not valid");
                    continue;
                }
                ok = false;
            }
            while (ok);
            do
            {
                if (Form_Login.InputPasswordBox("Confirm password", "Enter your password again: ", ref confirm) != DialogResult.OK)
                    return;
                if (!password.Equals(confirm))
                {
                    FileManager.showMessage("Error:\nPasswords mismatch");
                    continue;
                }
                currentUser.changePassword(password);
                FileManager.showMessage("Password is successfully changed");
                User.serialize(userlist);
                return;
            }
            while (true);
        }

        /*
         * 
         * ABOUT
         * 
         */
        void open_help(Object sender, EventArgs args)
        {
            try
            {
                System.Diagnostics.Process.Start("Readme.txt");
            }
            catch (Exception) { FileManager.showMessage("Error: Cannot open file readme.txt"); }
        }

        void open_author(Object sender, EventArgs args)
        {
            FileManager.showMessage("This is File Manager\ndeveloped by Nol Nick");
        }
    }
}
