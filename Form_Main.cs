﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;

namespace FileManager
{
    public partial class Form_Main : Form
    {
        const byte _col_lastmod_width = 100;
        const byte _col_type_width = 75;
        const byte _col_size_width = 50;
        const bool _LEFT = true;
        const bool _RIGHT = false;
        const string _parent_dir = "..";

        static string leftpath = string.Empty;
        static string rightpath = string.Empty;
        static User GUEST = User.getGuestInstance();

        List<ToolStripButton> commandlist;
        Form_Login log_in_form;
        public User currentUser = GUEST;
        public List<User> userlist;

        private void initDetails()
        {
            listView1.View = View.Details;
            listView2.View = View.Details;

            // initialize columns for detailed view
            listView1.Columns.Add("Name");
            listView1.Columns.Add("Last mod time", _col_lastmod_width);
            listView1.Columns.Add("Type", _col_type_width);
            listView1.Columns.Add("Size", _col_size_width);
            listView2.Columns.Add("Name");
            listView2.Columns.Add("Last mod time", _col_lastmod_width);
            listView2.Columns.Add("Type", _col_type_width);
            listView2.Columns.Add("Size", _col_size_width);

            // initialize tips for commands in bottom statusbar
            foreach (var command in commandlist)
            {
                statusStripCommands.Items.Add(command);
            }

            // initialize pathes in statusbars
            statusStripPath1.Items.Add("/");
            statusStripPath2.Items.Add("/");
            statusStripCmd.Items.Add("/");
            statusStripCmd.ShowItemToolTips = true;

            // initialize icons for listview items
            listView1.SmallImageList = new ImageList();
            listView1.SmallImageList.Images.Add("folder", Properties.Resources.folder);
            listView1.SmallImageList.Images.Add("Fixed", Properties.Resources.HD);
            listView1.SmallImageList.Images.Add("CDRom", Properties.Resources.cdrom);
            listView1.SmallImageList.Images.Add("Removable", Properties.Resources.usb);
            listView1.SmallImageList.Images.Add("EXE file", Properties.Resources.exe);
            listView1.SmallImageList.Images.Add("TXT file", Properties.Resources.text);
            listView1.SmallImageList.Images.Add("LNK file", Properties.Resources.link);

            listView2.SmallImageList = listView1.SmallImageList;

            /* 
             * 
             * link menu items to commands
             * 
             */
            // filesystem management commands
            leftToolStripMenuItem.Click += set_focus_left;
            rightToolStripMenuItem.Click += set_focus_right;
            openFileToolStripMenuItem.Click += view_file;
            renameToolStripMenuItem.Click += rename_entry;
            copyToolStripMenuItem.Click += copy_content;
            moveToolStripMenuItem.Click += move_content;
            deleteToolStripMenuItem.Click += delete_content;
            createFolderToolStripMenuItem.Click += create_dir;
            buttonRunCMD.Click += run_commandline;

            // view customisation commands
            backGroundToolStripMenuItem.Click += set_color_background;
            textToolStripMenuItem.Click += set_color_text;
            fontToolStripMenuItem.Click += set_text_font;

            // user management commands
            logInToolStripMenuItem.Click += show_log_in_panel;
            permissionsToolStripMenuItem.Click += show_admin_panel;
            changePasswordToolStripMenuItem.Click += change_password;

            // info output
            helpToolStripMenuItem.Click += open_help;
            authorToolStripMenuItem.Click += open_author;
        }

        private void adjustColumns()
        {
            ListView[] listview_array = { listView1, listView2 };
            foreach (var listview in listview_array)
            {
                if ((listview.View != View.Details) || (listview.Columns.Count < 1))
                    return;
                int space = listview.Width;
                space = Math.Max(space, 300);
                listview.Columns[0].Width = space - _col_lastmod_width - _col_type_width - _col_size_width - 5;
            }
        }

        private void refresh(bool panel)
        {
            ListView listview = panel ? listView1 : listView2;
            string current_dir = panel ? leftpath : rightpath;

            // save selected items
            string[] selected_entries = new string[listview.SelectedItems.Count];
            for (int i = 0; i < selected_entries.Length; i++)
            {
                selected_entries[i] = listview.SelectedItems[i].Text;
            }

            loadEntries(FileManager.openDirectory(current_dir), panel);

            // restore selected items
            foreach (ListViewItem item in listview.Items)
            {
                if (Array.Find(selected_entries, (str) => item.Text.Equals(str)) != null)
                    item.Selected = true;
            }
            colorize();
        }

        private void colorize()
        {
            listView1.BackColor = background_color;
            listView1.ForeColor = text_color;
            listView2.BackColor = background_color;
            listView2.ForeColor = text_color;
        }

        private void adjustCMD()
        {
            string cmd_path = FileManager.currentDir;
            if (cmd_path.Length > 35)
                cmd_path = cmd_path.Substring(0, 15) + "..." + cmd_path.Substring(cmd_path.Length - 15);
            statusStripCmd.Items[0].Text = cmd_path + " >";
            statusStripCmd.Items[0].ToolTipText = FileManager.currentDir;
        }

        private void showAllowedCommands()
        {
            // filesystem management commands
            renameToolStripMenuItem.Enabled = currentUser.hasPermission(User.permissions.RENAME);
            copyToolStripMenuItem.Enabled = currentUser.hasPermission(User.permissions.COPY);
            moveToolStripMenuItem.Enabled = currentUser.hasPermission(User.permissions.MOVE);
            deleteToolStripMenuItem.Enabled = currentUser.hasPermission(User.permissions.REMOVE);
            createFolderToolStripMenuItem.Enabled = currentUser.hasPermission(User.permissions.CREATE);

            foreach (ToolStripButton button in commandlist)
            {
                button.Enabled = true;
            }
            foreach (ToolStripButton button in commandlist)
            {
                if (button.Text.Contains("Rename"))
                    button.Enabled = currentUser.hasPermission(User.permissions.RENAME);
                if (button.Text.Contains("Copy"))
                    button.Enabled = currentUser.hasPermission(User.permissions.COPY);
                if (button.Text.Contains("Move"))
                    button.Enabled = currentUser.hasPermission(User.permissions.MOVE);
                if (button.Text.Contains("Delete"))
                    button.Enabled = currentUser.hasPermission(User.permissions.REMOVE);
                if (button.Text.Contains("New"))
                    button.Enabled = currentUser.hasPermission(User.permissions.CREATE);
            }

            // user management commands
            permissionsToolStripMenuItem.Enabled = currentUser.hasPermission(User.permissions.ADMINISTRATE);
            changePasswordToolStripMenuItem.Visible = !currentUser.Equals(GUEST);

            // Command line
            textBox1.Enabled = currentUser.hasPermission(User.permissions.COMMANDLINE);
            buttonRunCMD.Enabled = currentUser.hasPermission(User.permissions.COMMANDLINE);
        }

        private void init()
        {
            FileManager.ask = (str) =>
            {
                return MessageBox.Show(str, "File Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                    == System.Windows.Forms.DialogResult.Yes;
            };
            FileManager.showMessage = (str) =>
                MessageBox.Show(str, "File Manager", MessageBoxButtons.OK, MessageBoxIcon.Information);

            commandlist = new List<ToolStripButton>();
            log_in_form = new Form_Login(this);
            userlist = User.deserialize();

            initCommands();
            initCustomization();
            initDetails();
            loadEntries(FileManager.getDrives(), _RIGHT);
            loadEntries(FileManager.getDrives(), _LEFT);
            adjustColumns();
            showAllowedCommands();
        }

        private void loadEntries(EntryInfo[] entries, bool panel)
        {
            StatusStrip status = (panel == _LEFT) ? statusStripPath1 : statusStripPath2;
            ListView listview = (panel == _LEFT) ? listView1 : listView2;
            ListViewItem item;

            if (entries == null)
                return;

            status.Items[0].Text = FileManager.currentDir;
            listview.Items.Clear();

            if (!FileManager.isRoot)
            {
                listview.Items.Add(_parent_dir);    // add link to parent directory
                listview.LabelEdit = true;          // enable "rename entry"
            }
            else
                listview.LabelEdit = false;

            foreach (var entry in entries)
            {
                item = new ListViewItem(entry.getFields(), entry.type);
                listview.Items.Add(item);
            }

            if (panel == _LEFT)
                leftpath = FileManager.currentDir;
            else
                rightpath = FileManager.currentDir;
            colorize();
            adjustCMD();
        }

        public Form_Main()
        {
            InitializeComponent();
        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            init();
#if DEBUG
            User admin = userlist.Find( (usr) => usr.name.Equals("admin") );
            set_logined_user(admin);
#endif
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            adjustColumns();
        }

        private void Form_Main_ResizeEnd(object sender, EventArgs e)
        {
            adjustColumns();
            textBox1.Location = new System.Drawing.Point(statusStripCmd.Location.X + 200, statusStripCmd.Location.Y + 1);
            textBox1.Width = statusStripCmd.Width - 200 - buttonRunCMD.Width - 10;
            buttonRunCMD.Location = new System.Drawing.Point(textBox1.Location.X + textBox1.Width, textBox1.Location.Y);
        }

        private void listView1_Enter(object sender, EventArgs e)
        {
            bool panel = listView1.Focused;
            if (panel == _LEFT)
                set_focus_left(sender, e);
            else
                set_focus_right(sender, e);
        }

        // open directory
        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ListView listview = sender as ListView;
            bool panel = listview.Equals(listView1);

            if (listview.SelectedItems.Count == 0)
                return;
            var entry = listview.SelectedItems[0].Text;
            if (listview.Items[0].Selected && (listview.Items[0].Text.Equals(_parent_dir)))
                loadEntries(FileManager.getParent(), panel);
            else
                if (FileManager.isDirectory(entry))
                    loadEntries(FileManager.openDirectory(entry), panel);
                else
                    view_file(sender, e);
        }

        // prevent renaming of system entries
        private void listView1_BeforeLabelEdit(object sender, LabelEditEventArgs e)
        {
            // cannot rename link to parent directory
            if (e.Item == 0)
            {
                e.CancelEdit = true;
                return;
            }

            ListView listview = sender as ListView;
            ListViewItem item = listview.Items[e.Item];
            string path = item.Text;

            if (FileManager.isSystem(path))
            {
                e.CancelEdit = true;
                FileManager.showMessage("You cannot rename system files.");
            }
        }

        // rename entry associated with edited item
        private void listView1_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            ListView listview = sender as ListView;
            string path = listview.Items[e.Item].Text;

            if (path.Equals(e.Label) || string.IsNullOrEmpty(e.Label) || !FileManager.isValidName(e.Label))
            {
                e.CancelEdit = true;
            }
            else
            {
                FileManager.renameEntry(path, e.Label);
            }
            if (leftpath.Equals(rightpath))
                refresh(!listview.Equals(listView1));
            listview.Focus(); // restore focus
        }

        // Add key bindings for some non-command operations
        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            ListView listview = sender as ListView;

            // Enter or Space
            // Opens entry
            if (e.KeyCode.Equals(Keys.Enter) || e.KeyCode.Equals(Keys.Space))
            {
                bool dir_found = false;
                if (listview.SelectedItems.Count == 0)
                    return;
                foreach (ListViewItem entry in listview.SelectedItems)
                {
                    if (FileManager.isDirectory(entry.Text) && !dir_found)
                    {
                        bool panel = listview.Equals(listView1);
                        if (entry.Text.Equals(_parent_dir))
                            loadEntries(FileManager.getParent(), panel);
                        else
                            loadEntries(FileManager.openDirectory(entry.Text), panel);
                        dir_found = true;
                    }
                    else
                    {
                        view_file(sender, e);
                    }
                }
            }
            // Backspace
            // Return to the parent directory
            else if (e.KeyCode.Equals(Keys.Back))
            {
                bool panel = listview.Equals(listView1);
                if (!FileManager.isRoot)
                    loadEntries(FileManager.getParent(), panel);
            }
            // Delete
            // Remove entries
            else if (e.KeyCode.Equals(Keys.Delete))
            {
                delete_content(sender, e);
            }
        }
    }
}
