﻿namespace FileManager
{
    partial class Form_admin_panel
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxUser = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxMove = new System.Windows.Forms.CheckBox();
            this.checkBoxCopy = new System.Windows.Forms.CheckBox();
            this.checkBoxRename = new System.Windows.Forms.CheckBox();
            this.checkBoxRemove = new System.Windows.Forms.CheckBox();
            this.checkBoxCreate = new System.Windows.Forms.CheckBox();
            this.checkBoxCMD = new System.Windows.Forms.CheckBox();
            this.checkBoxAdmin = new System.Windows.Forms.CheckBox();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.buttonAll = new System.Windows.Forms.Button();
            this.buttonCommon = new System.Windows.Forms.Button();
            this.buttonNone = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxUser
            // 
            this.listBoxUser.FormattingEnabled = true;
            this.listBoxUser.Location = new System.Drawing.Point(15, 38);
            this.listBoxUser.MultiColumn = true;
            this.listBoxUser.Name = "listBoxUser";
            this.listBoxUser.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listBoxUser.Size = new System.Drawing.Size(129, 199);
            this.listBoxUser.Sorted = true;
            this.listBoxUser.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(33, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select users";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonNone);
            this.groupBox1.Controls.Add(this.buttonCommon);
            this.groupBox1.Controls.Add(this.buttonAll);
            this.groupBox1.Controls.Add(this.checkBoxAdmin);
            this.groupBox1.Controls.Add(this.checkBoxCMD);
            this.groupBox1.Controls.Add(this.checkBoxCreate);
            this.groupBox1.Controls.Add(this.checkBoxRemove);
            this.groupBox1.Controls.Add(this.checkBoxRename);
            this.groupBox1.Controls.Add(this.checkBoxCopy);
            this.groupBox1.Controls.Add(this.checkBoxMove);
            this.groupBox1.Location = new System.Drawing.Point(182, 16);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 221);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Permissions";
            // 
            // checkBoxMove
            // 
            this.checkBoxMove.AutoSize = true;
            this.checkBoxMove.Location = new System.Drawing.Point(21, 25);
            this.checkBoxMove.Name = "checkBoxMove";
            this.checkBoxMove.Size = new System.Drawing.Size(110, 17);
            this.checkBoxMove.TabIndex = 0;
            this.checkBoxMove.Text = "Move files/folders";
            this.checkBoxMove.UseVisualStyleBackColor = true;
            // 
            // checkBoxCopy
            // 
            this.checkBoxCopy.AutoSize = true;
            this.checkBoxCopy.Location = new System.Drawing.Point(21, 48);
            this.checkBoxCopy.Name = "checkBoxCopy";
            this.checkBoxCopy.Size = new System.Drawing.Size(107, 17);
            this.checkBoxCopy.TabIndex = 1;
            this.checkBoxCopy.Text = "Copy files/folders";
            this.checkBoxCopy.UseVisualStyleBackColor = true;
            // 
            // checkBoxRename
            // 
            this.checkBoxRename.AutoSize = true;
            this.checkBoxRename.Location = new System.Drawing.Point(21, 72);
            this.checkBoxRename.Name = "checkBoxRename";
            this.checkBoxRename.Size = new System.Drawing.Size(123, 17);
            this.checkBoxRename.TabIndex = 2;
            this.checkBoxRename.Text = "Rename files/folders";
            this.checkBoxRename.UseVisualStyleBackColor = true;
            // 
            // checkBoxRemove
            // 
            this.checkBoxRemove.AutoSize = true;
            this.checkBoxRemove.Location = new System.Drawing.Point(21, 96);
            this.checkBoxRemove.Name = "checkBoxRemove";
            this.checkBoxRemove.Size = new System.Drawing.Size(123, 17);
            this.checkBoxRemove.TabIndex = 3;
            this.checkBoxRemove.Text = "Remove files/folders";
            this.checkBoxRemove.UseVisualStyleBackColor = true;
            // 
            // checkBoxCreate
            // 
            this.checkBoxCreate.AutoSize = true;
            this.checkBoxCreate.Location = new System.Drawing.Point(21, 120);
            this.checkBoxCreate.Name = "checkBoxCreate";
            this.checkBoxCreate.Size = new System.Drawing.Size(108, 17);
            this.checkBoxCreate.TabIndex = 4;
            this.checkBoxCreate.Text = "Create directories";
            this.checkBoxCreate.UseVisualStyleBackColor = true;
            // 
            // checkBoxCMD
            // 
            this.checkBoxCMD.AutoSize = true;
            this.checkBoxCMD.Location = new System.Drawing.Point(21, 144);
            this.checkBoxCMD.Name = "checkBoxCMD";
            this.checkBoxCMD.Size = new System.Drawing.Size(110, 17);
            this.checkBoxCMD.TabIndex = 5;
            this.checkBoxCMD.Text = "Use commandline";
            this.checkBoxCMD.UseVisualStyleBackColor = true;
            // 
            // checkBoxAdmin
            // 
            this.checkBoxAdmin.AutoSize = true;
            this.checkBoxAdmin.Location = new System.Drawing.Point(21, 168);
            this.checkBoxAdmin.Name = "checkBoxAdmin";
            this.checkBoxAdmin.Size = new System.Drawing.Size(143, 17);
            this.checkBoxAdmin.TabIndex = 6;
            this.checkBoxAdmin.Text = "Change user permissions";
            this.checkBoxAdmin.UseVisualStyleBackColor = true;
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(24, 253);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(129, 29);
            this.buttonApply.TabIndex = 3;
            this.buttonApply.Text = "Apply permissions";
            this.buttonApply.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(297, 253);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(85, 29);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "OK";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonDefault
            // 
            this.buttonDefault.Location = new System.Drawing.Point(159, 253);
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.Size = new System.Drawing.Size(123, 29);
            this.buttonDefault.TabIndex = 5;
            this.buttonDefault.Text = "Apply default";
            this.buttonDefault.UseVisualStyleBackColor = true;
            // 
            // buttonAll
            // 
            this.buttonAll.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonAll.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonAll.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAll.Location = new System.Drawing.Point(14, 192);
            this.buttonAll.Margin = new System.Windows.Forms.Padding(0);
            this.buttonAll.Name = "buttonAll";
            this.buttonAll.Size = new System.Drawing.Size(55, 23);
            this.buttonAll.TabIndex = 7;
            this.buttonAll.Text = "all";
            this.buttonAll.UseVisualStyleBackColor = false;
            // 
            // buttonCommon
            // 
            this.buttonCommon.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonCommon.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonCommon.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonCommon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCommon.Location = new System.Drawing.Point(68, 192);
            this.buttonCommon.Margin = new System.Windows.Forms.Padding(0);
            this.buttonCommon.Name = "buttonCommon";
            this.buttonCommon.Size = new System.Drawing.Size(64, 23);
            this.buttonCommon.TabIndex = 8;
            this.buttonCommon.Text = "common";
            this.buttonCommon.UseVisualStyleBackColor = false;
            // 
            // buttonNone
            // 
            this.buttonNone.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.buttonNone.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.buttonNone.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonNone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNone.Location = new System.Drawing.Point(131, 192);
            this.buttonNone.Margin = new System.Windows.Forms.Padding(0);
            this.buttonNone.Name = "buttonNone";
            this.buttonNone.Size = new System.Drawing.Size(57, 23);
            this.buttonNone.TabIndex = 9;
            this.buttonNone.Text = "none";
            this.buttonNone.UseVisualStyleBackColor = false;
            // 
            // Form_admin_panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 301);
            this.Controls.Add(this.buttonDefault);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form_admin_panel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "User management";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxUser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxMove;
        private System.Windows.Forms.CheckBox checkBoxCopy;
        private System.Windows.Forms.CheckBox checkBoxRename;
        private System.Windows.Forms.CheckBox checkBoxRemove;
        private System.Windows.Forms.CheckBox checkBoxCreate;
        private System.Windows.Forms.CheckBox checkBoxCMD;
        private System.Windows.Forms.CheckBox checkBoxAdmin;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonDefault;
        private System.Windows.Forms.Button buttonAll;
        private System.Windows.Forms.Button buttonNone;
        private System.Windows.Forms.Button buttonCommon;
    }
}