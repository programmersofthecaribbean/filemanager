﻿using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace FileManager
{
    [Serializable]
    public class User
    {
        string login;
        string password;
        bool isAdmin;
        byte flags;

        public string name { get { return login; } }
        public bool is_admin { get { return isAdmin; } }

        public enum user_status { 
            EXISTS, 
            CREATED, 
            ADDED 
        };
        
        // Flags for user permissions
        public struct permissions
        {
            // Flags for individual commands
            public static byte MOVE = 1;
            public static byte COPY = 2;
            public static byte RENAME = 4;
            public static byte REMOVE = 8;
            public static byte CREATE = 16;
            public static byte COMMANDLINE = 32;
            public static byte ADMINISTRATE = 64;
            
            // Flags for users
            public static byte ALL = 255;
            public static byte COMMON = 3;
            public static byte GUEST = 0;
        }

        private User(string name, string pass, bool admin = false)
        {
            login = name;
            password = pass;
            isAdmin = admin;
            if (isAdmin)
                flags = permissions.ALL;
            else
                flags = permissions.COMMON;
        }

        // write list to binary file
        public static void serialize(List<User> userlist)
        {
            try
            {
                using (var outstream = File.Open("user.dat", FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(outstream, userlist);
                }
            }
            catch (IOException e)
            {
                FileManager.showMessage("I/O Error:\n" + e.Message);
            }
        }

        // read list from binary file
        public static List<User> deserialize()
        {
            List<User> studentlist = new List<User>();
            try
            {
                using (var instream = File.Open("user.dat", FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    studentlist = (List<User>)bin.Deserialize(instream);
                }
            }
            catch (IOException e)
            {
                FileManager.showMessage("I/O Error:\n" + e.Message);
            }
            return studentlist;
        }

        // set user permissions
        public void setPermissions(byte flags)
        {
            this.flags = flags;
        }

        // get permission flag
        public bool hasPermission(byte flags)
        {
            return (this.flags & flags) == flags;
        }

        // change password
        public void changePassword(string newpass)
        {
            password = newpass;
        }

        // create new or find existing user
        public static user_status createUser(List<User> userlist, out User retUser, string name, string pass, bool admin = false)
        {
            retUser = userlist.Find((user) => user.login.Equals(name));
            if (retUser != null)
                return user_status.EXISTS;
            else
            {
                retUser = new User(name, pass, admin);
                if (userlist == null)
                    return user_status.CREATED;
                userlist.Add(retUser);
                return user_status.ADDED;
            }
        }

        public static User getGuestInstance()
        {
            User guest = new User("", "");
            guest.setPermissions(permissions.GUEST);
            return guest;
        }

        // login validation
        public static bool isValidUser(List<User> userlist, out User currentUser, string name, string pwrd)
        {
            currentUser = userlist.Find((user) => (user.login.Equals(name) && user.password.Equals(pwrd)));
            return currentUser != null;
        }

        public static bool isValidString(string name)
        {
            bool isValid = !string.IsNullOrEmpty(name) && (name.IndexOfAny(Path.GetInvalidFileNameChars()) < 0);
            return isValid;
        }
    }
}
