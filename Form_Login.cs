﻿using System.Windows.Forms;
using System;
using System.Drawing;

namespace FileManager
{
    public partial class Form_Login : Form
    {
        Form_Main parent;

        public static DialogResult InputPasswordBox(string title, string promptText, ref string value)
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = title;
            label.Text = promptText;
            textBox.Text = value;

            textBox.PasswordChar = '*';
            textBox.UseSystemPasswordChar = true;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }


        public Form_Login()
        {
            InitializeComponent();
        }

        public Form_Login(Form parent)
        {
            this.parent = parent as Form_Main;
            InitializeComponent();
            button_login.Click += log_in_user;
            button_reg.Click += register_user;
        }

        void log_in_user(Object sender, EventArgs args)
        {
            string username = textBox1.Text;
            string password = textBox2.Text;
            User newUser;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                FileManager.showMessage("Authorization error:\nEmpty parameters.");
                return;
            }
            if (!User.isValidUser(parent.userlist, out newUser, username, password))
            {
                FileManager.showMessage("Authorization error:\nWrong username or password.");
                return;
            }

            parent.set_logined_user(newUser);

            // clear data
            textBox1.Clear();
            textBox2.Clear();
            Close();
        }

        void register_user(Object sender, EventArgs args)
        {
            string username = textBox1.Text;
            string password = textBox2.Text;
            string confirm_pass = "";
            User newUser;

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                FileManager.showMessage("Registration error:\nEnter all parameters for registration.");
                return;
            }
            if (!User.isValidString(username) || !User.isValidString(password))
                return;
            if (InputPasswordBox("User registration", "Confirm your password", ref confirm_pass) != DialogResult.OK)
                return;
            if (!password.Equals(confirm_pass))
            {
                FileManager.showMessage("Registration error:\nPasswords mismatch.");
                textBox2.Clear();
                return;
            }
            var result = User.createUser(parent.userlist, out newUser, username, password);
            if (result == User.user_status.EXISTS)
            {
                FileManager.showMessage("Registration error:\nSuch user already exists.");
                return;
            }
            else if (result == User.user_status.ADDED)
                User.serialize(parent.userlist);
            else
                FileManager.showMessage("Database error:\nCannot add user to list.");

            parent.set_logined_user(newUser);

            // clear data
            textBox1.Clear();
            textBox2.Clear();
            Close();
        }
    }
}
