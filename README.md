*****************************************************************************
*  Welcome to File Manager 1.0.1!                                           
*  This README file contains important information about FileManager.       
*  For the latest information about this application and its accompanying   
*  programs and manuals, read this file in its entirety.                    
*  To obtain the current release, visit                                     
*  https://bitbucket.org/programmersofthecaribbean/filemanager/downloads    
*****************************************************************************

TABLE OF CONTENTS
-----------------
1.  System Requirements
2.  Installation notes
3.  Features
4.  Changelog
5.  Corrections to the Documents

------------------------------------------------------------
 1. System Requirements
------------------------------------------------------------

- Microsoft Windows 2000 (Service Pack 2 or higher), XP, Vista, Windows Seven,Windows 8 or 8.1
- Microsoft .NET Framework 3.5 SP1
- Pentium compatible PC (Pentium III or Athlon recommended) or higher
- 256 MB RAM (512 MB RAM recommended)
- 1 MB available hard disk space
- 1024x768 resolution (higher resolution recommended), at least 256 colors
- NTFS,FAT32 file systems.

------------------------------------------------------------
 2. Installation notes
------------------------------------------------------------

You dont need to install this product. First you need to extract the downloaded archive.
Then just run executable file ("FileManager.exe") and feel the power.


------------------------------------------------------------
 3. Features
------------------------------------------------------------

- Show list of files.
- Icons for home (root) directory and SD card.
- Directory structure displayed through clickablebuttons.
- Support for many file endings and mime types.
- Create directory, rename, delete files.
- Move files.
- Work with command line
- User interface flexibility

------------------------------------------------------------
 4. Changelog
------------------------------------------------------------

beta: 0.6.0
date: 2014-10-2
- Add user interface
- Add commands "copy","move","rename","delete".


beta: 0.8.0
date: 2014-10-16
- Add registration
- Add autorization
- Add user interface customization
- Add icons
- Add second panel

beta: 0.9.0
date: 2014-11-2
- Add command line
- Add help
- Rebuild user interface 
- Fix some minor bugs

release: 1.0.0
date: 2014-11-12
- First public release


release: 1.0.1
date: 2014-11-15
- Fix some minor bugs


------------------------------------------------------------
 5. Corrections to the Documents
------------------------------------------------------------

Please see the readme.txt file in the main directory for corrections
to the document.