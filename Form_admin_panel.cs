﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace FileManager
{
    public partial class Form_admin_panel : Form
    {
        Form_Main parent;

        public Form_admin_panel()
        {
            InitializeComponent();
        }

        public Form_admin_panel(Form parent)
        {
            this.parent = parent as Form_Main;
            InitializeComponent();
            // link components with events
            buttonCancel.Click += (sender, args) => Close();
            buttonApply.Click += applySelected;
            buttonDefault.Click += applyDefault;
            buttonNone.Click += setNone;
            buttonCommon.Click += setCommon;
            buttonAll.Click += setAll;
            listBoxUser.DoubleClick += loadPermissions;
            this.Load += init;
        }

        void init(Object sender, EventArgs args)
        {
            foreach (var user in parent.userlist)
            {
                if (!user.name.Equals(parent.currentUser.name))
                    listBoxUser.Items.Add(user.name);
            }
        }

        void setNone(Object sender, EventArgs args)
        {
            foreach (CheckBox control in groupBox1.Controls.OfType<CheckBox>())
            {
                control.Checked = false;
            }
        }

        void setAll(Object sender, EventArgs args)
        {
            foreach (CheckBox control in groupBox1.Controls.OfType<CheckBox>())
            {
                control.Checked = true;
            }
        }

        void setCommon(Object sender, EventArgs args)
        {
            setNone(sender, args);
            checkBoxMove.Checked = true;
            checkBoxCopy.Checked = true;
        }

        void loadPermissions(Object sender, EventArgs args)
        {
            string usrname = (string)listBoxUser.SelectedItem;
            User user = parent.userlist.Find((usr) => usr.name.Equals(usrname));
            checkBoxAdmin.Checked = user.hasPermission(User.permissions.ADMINISTRATE);
            checkBoxCMD.Checked = user.hasPermission(User.permissions.COMMANDLINE);
            checkBoxCopy.Checked = user.hasPermission(User.permissions.COPY);
            checkBoxCreate.Checked = user.hasPermission(User.permissions.CREATE);
            checkBoxMove.Checked = user.hasPermission(User.permissions.MOVE);
            checkBoxRemove.Checked = user.hasPermission(User.permissions.REMOVE);
            checkBoxRename.Checked = user.hasPermission(User.permissions.RENAME);
        }

        void applySelected(Object sender, EventArgs args)
        {
            byte flags = 0;
            if (checkBoxAdmin.Checked)
                flags |= User.permissions.ADMINISTRATE;
            if (checkBoxCMD.Checked)
                flags |= User.permissions.COMMANDLINE;
            if (checkBoxCopy.Checked)
                flags |= User.permissions.COPY;
            if (checkBoxCreate.Checked)
                flags |= User.permissions.CREATE;
            if (checkBoxMove.Checked)
                flags |= User.permissions.MOVE;
            if (checkBoxRemove.Checked)
                flags |= User.permissions.REMOVE;
            if (checkBoxRename.Checked)
                flags |= User.permissions.RENAME;

            foreach (var usrname in listBoxUser.SelectedItems)
            {
                User user = parent.userlist.Find((usr) => usr.name.Equals(usrname));
                if (user != null)
                    user.setPermissions(flags);
            }
            User.serialize(parent.userlist);
            FileManager.showMessage("Permissions successfully applied");
        }

        void applyDefault(Object sender, EventArgs args)
        {
            foreach (var usrname in listBoxUser.SelectedItems)
            {
                User user = parent.userlist.Find((usr) => usr.name.Equals(usrname));
                if (user != null)
                {
                    if (user.is_admin)
                        user.setPermissions(User.permissions.ALL);
                    else
                        user.setPermissions(User.permissions.COMMON);
                }
            }
            User.serialize(parent.userlist);
            FileManager.showMessage("Default permissions successfully applied");
        }
    }
}
